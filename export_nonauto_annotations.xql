xquery version "3.1";

(: Find all the annotations that are not autocorrect regardless of status and dump them to
 : a document that can be exported, then delete them.  By retrieving annotations that were
 : not modified by autocorrect, we include those that were created by autocorrect and
 : approved by a human being.  For now we will leave comments as they cannot be processed
 : in any automated fashion.
 :)

(# exist:batch-transaction #) {
let $source := '/db/apps/EarlyPrintAnnotations/annotations'

let $col := if (xmldb:collection-available("/db/apps/EarlyPrintAnnotations/annotations-export"))
            then "/db/apps/EarlyPrintAnnotations/annotations-export"
            else xmldb:create-collection("/db/apps/EarlyPrintAnnotations/", "annotations-export")

let $doc := "annotations_export_" || replace(xs:string(current-dateTime()), '[:\-\.]+', '_') || ".xml"
let $record := element annotation-list { () }
let $output := xmldb:store($col, $doc, $record)
let $list := doc($output)//annotation-list

let $result :=
    for $d in collection($source)
        for $anno in $d//annotation-list/annotation-item[@modifier != 'autocorrect']
            return
            if ($anno/annotation-body/@subtype = 'comment') 
            then () 
            else 
                ($anno,
                update insert $anno into $list,
                update delete $anno)

return "Exported " || count($result) || " annotations."
} (: end of transaction :)

