xquery version "3.1";

(: Find all the annotations that are not pending and dump them to
 : a document that can be exported, then delete them.
 :)

(# exist:batch-transaction #) {
let $source := '/db/apps/EarlyPrintAnnotations/annotations'

let $col := if (xmldb:collection-available("/db/apps/EarlyPrintAnnotations/annotations-export"))
            then "/db/apps/EarlyPrintAnnotations/annotations-export"
            else xmldb:create-collection("/db/apps/EarlyPrintAnnotations/", "annotations-export")

let $doc := "annotations_export_" || replace(xs:string(current-dateTime()), '[:\-\.]+', '_') || ".xml"
let $record := element annotation-list { () }
let $output := xmldb:store($col, $doc, $record)
let $list := doc($output)//annotation-list

let $result :=
    for $d in collection($source)
        for $anno in $d//annotation-list/annotation-item[@status != 'pending']
            return
            ($anno,
             update insert $anno into $list,
             update delete $anno)

return "Exported " || count($result) || " annotations."
} (: end of transaction :)

