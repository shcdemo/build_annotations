# Annotation Package

This repository is used to build an empty package that will hold user-produced
annotations for the Early Print project.

## EarlyPrintAnnotations Package Creation

The first time you set up a new instance of the application, you must create
and install the EarlyPrintAnnotations package.  The package contains indexing
information and other metadata, as well as an empty annotations collection.  To
create the package, just run ant like so:

    $ ant

Then use the eXist package manager to load the resulting .xar file from the
build/ directory.

This is only necessary once when initially setting up a new instance of the
Early Print annotation module.  The annotation service will automatically
create text-specific sub-collections and annotations files within this
collection as users produce the annotations.

The package includes the export_annotations.xql script that will create a
single file containing all annotations to date.  This file is the beginnings
of a process for downstream application of the annotations.