xquery version "3.1";

import module namespace annotation="http://existsolutions.com/annotation-service/annotation" at "/db/apps/annotation-service/modules/annotation.xql";

let $anno-root := "/db/apps/EarlyPrintAnnotations/annotations"
let $import-file := "/db/apps/EarlyPrintAnnotations/annotations_import.xml"

let $result :=
for $anno in doc($import-file)//annotation-item
    let $document-id := $anno//annotation-target/@source
    let $path := $anno-root || '/' || substring($document-id, 1, 3) || '/' || $document-id || '_annotations.xml'
    let $doc := if(doc($path)) then doc($path) else doc(annotation:setup($document-id))
    let $item :=
        if (count($doc//annotation-item[@id=$anno/@id]) = 0) then (
            element li {
                attribute type { 'create' },
                'Creating ' || $anno/@id || ' in ' || $path,
                update insert $anno into $doc//annotation-list
            }
        )
        else (
            element li {
                attribute type { 'skip' },
                 'Skipping ' || $anno/@id || ' which already exists in ' || $path
            }
        )
    return  $item

return element ul { $result }
        

